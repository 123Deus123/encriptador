﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Models;
using Models.Modelos;
using Models.Models;

namespace InitDb.Inicializadores
{
    public class InitUsuario
    {
        public void InitDb(MContext con)
        {
            Documentos documentos = new Documentos();
            documentos.cedula = "1065622714";
            documentos.ciudad_nacimiento = "1990-05-13";
            documentos.fecha_expedicion = "2009-05-13";
            con.Documentos.Add(documentos);
            Usuario user = new Usuario();
            user.Users = "eduardo";
            user.Password = EncodePass("1234");
            con.Usuario.Add(user);
            con.SaveChanges();
        }

        public static string EncodePass(string password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }
    }
}
