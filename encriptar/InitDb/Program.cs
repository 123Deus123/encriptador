﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InitDb.Inicializadores;
using Models;

namespace InitDb
{
    class Program
    {
        static void Main(string[] args)
        {
            MContext con = new MContext();
            Console.WriteLine("Iniciando base de datos...");
            #region crear tablas
            con.Usuario.Create();
            con.Documentos.Create();
            #endregion
            InitUsuario initusuario = new InitUsuario();
            initusuario.InitDb(con);
            con.SaveChanges();
            Console.WriteLine("Terminado, pulse una tecla");
            Console.ReadKey();
        }
    }
}
