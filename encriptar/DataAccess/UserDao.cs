﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess
{
    public class UserDao:ConnectionToSql
    {
        public bool Login(string User, string Pass)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "select *from LoginUsers WHERE Users=@Users and Passwords=@Pass";
                    command.Parameters.AddWithValue("@Users",User);
                    command.Parameters.AddWithValue("@Pass", Pass);
                    command.CommandType = CommandType.Text;
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        return true;   //Consulta exitosa
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}
