﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace encriptar
{
    public partial class Principal : Form
    {
        String NOMBRE;
        private bool moviendoFormulario;
        private Point posicionActualPuntero;

        public Principal()
        {
            InitializeComponent();
        }

        

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            using (var fd = new OpenFileDialog())
            { 
              
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fd.FileName))
                {
                    txtDirectorio.Text = fd.FileName;
                    NOMBRE = fd.FileName.Remove(0, fd.FileName.LastIndexOf("\"") + 1);
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            using (var fd = new SaveFileDialog())
            {
                fd.FileName = NOMBRE;
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fd.FileName))
                {
                    txtGuardar.Text = fd.FileName;
                }
            }
        }

        private void btnEncriptar_Click(object sender, EventArgs e)
        {
            try
            {
                //PONE EL ARCHIVO A CONVERTIR EN BYTES
                byte[] Archivo = File.ReadAllBytes(txtDirectorio.Text);
                int Tamaño = Archivo.Length;

                //PREPARA LA CLAVE PARA LA ENCRIPTACION
                MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
                byte[] Clave =  MD5.ComputeHash(Encoding.ASCII.GetBytes("CONTRASEÑA"));
                MD5.Clear();

                //DEFINE EL TIPO DE ENCRIPTADO
                TripleDESCryptoServiceProvider TDC = new TripleDESCryptoServiceProvider();
                TDC.Key = Clave;
                TDC.Mode = CipherMode.ECB;

                //CREA E INICIALIZA EL STREAM PARA EL DESTINO
                FileStream Destino = new FileStream(txtGuardar.Text, FileMode.OpenOrCreate, FileAccess.Write);

                //CREA E INICIALIZA EL STREAM PARA LA ENCRIPTACION
                CryptoStream Encriptado = new CryptoStream(Destino, TDC.CreateEncryptor(), CryptoStreamMode.Write);

                //ENCRIPTA
                Encriptado.Write(Archivo, 0, Tamaño);
                Destino.Close();
                MessageBox.Show("Archivo Encripotado Correctamente");
            }
            catch 
            {
                MessageBox.Show("Error de Encriptación");
            }
        }       

        private void btnDesencriptar_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] Archivo = File.ReadAllBytes(txtDirectorio.Text);
                int Tamaño = Archivo.Length;

                MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();
                byte[] Clave = MD5.ComputeHash(Encoding.ASCII.GetBytes("CONTRASEÑA"));
                MD5.Clear();

                TripleDESCryptoServiceProvider TDC = new TripleDESCryptoServiceProvider();
                TDC.Key = Clave;
                TDC.Mode = CipherMode.ECB;

                FileStream Destino = new FileStream(txtGuardar.Text, FileMode.OpenOrCreate, FileAccess.Write);

                CryptoStream Encriptado = new CryptoStream(Destino, TDC.CreateDecryptor(), CryptoStreamMode.Write);

                Encriptado.Write(Archivo, 0, Tamaño);
                Destino.Close();
                MessageBox.Show("Archivo Desencriptado Correctamente");
            }
            catch
            {
                MessageBox.Show("Desencriptación fallida");
            }
        }

        private void pictureX_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureX_MouseHover(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_x_red.png");
            pictureX.BackgroundImage = image;
        }

        private void pictureX_MouseLeave(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_x_blue.png");
            pictureX.BackgroundImage = image;
        }

        private void pictureM_MouseHover(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_min_dark_blue.png");
            pictureM.BackgroundImage = image;
        }

        private void pictureM_MouseLeave(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_min_blue.png");
            pictureM.BackgroundImage = image;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                moviendoFormulario = true;
                posicionActualPuntero = new Point(e.X, e.Y);
            }
            else
            {
                moviendoFormulario = false;
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (moviendoFormulario)
            {
                Point nuevoPunto;

                nuevoPunto = PointToScreen(new Point(e.X, e.Y));
                nuevoPunto.Offset(-posicionActualPuntero.X, -posicionActualPuntero.Y);

                Location = nuevoPunto;
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            {
                moviendoFormulario = false;
            }
        }
    }
}
