﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dominio;

namespace encriptar
{
    public partial class Form1 : Form
    {
        private bool moviendoFormulario;
        private Point posicionActualPuntero;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {            
            if(txtUser.Text != "USUARIO")
            {
                if(txtPass.Text != "CONTRASEÑA") {
                    UserModel user = new UserModel();
                    var validLogin = user.LoginUser(txtUser.Text, txtPass.Text);
                    if (validLogin == true)
                    {
                        this.Hide();
                        Principal v1 = new Principal();
                        v1.Show();
                    }
                    else { MessageBox.Show("Usuario incorrecto"); }
                }
                else { MessageBox.Show("Por Favor ingrese Contraseña de Usuario"); }
            }
            else
            {
                MessageBox.Show("Por Favor ingrese Nombre de Usuario");
            }
        }

        private void Usuario_Text(object sender, EventArgs e)
        {
            if (txtUser.Text == "USUARIO") {
                txtUser.Text = "";
                txtUser.ForeColor = Color.Black;
            }
        }

        private void Usuario_Text_leave(object sender, EventArgs e)
        {
            if (txtUser.Text == "")
            {
                txtUser.Text = "USUARIO";
                txtUser.ForeColor = Color.DarkGray;
            }
        }

        private void Password_Text_Enter(object sender, EventArgs e)
        {
            if (txtPass.Text == "CONTRASEÑA")
            {
                txtPass.Text = "";
                txtPass.ForeColor = Color.Black;
                txtPass.PasswordChar = '*'; 
            }
        }

        private void Password_Text_Leave(object sender, EventArgs e)
        {
            if (txtPass.Text == "")
            {
                txtPass.Text = "CONTRASEÑA";
                txtPass.ForeColor = Color.DarkGray;
                txtPass.PasswordChar = '\0';
            }
        }

        private void pictureX_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureM_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pictureX_MouseHover(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_x_red.png");
           pictureX.BackgroundImage = image;
        }

        private void pictureX_MouseLeave(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_x_blue.png");
            pictureX.BackgroundImage = image;
        }

        private void pictureM_MouseHover(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_min_dark_blue.png");
            pictureM.BackgroundImage = image;
        }

        private void pictureM_MouseLeave(object sender, EventArgs e)
        {
            Image image = new Bitmap("C:/Users/Carlos/source/repos/Encripta/encriptar/encriptar/Resources/button_min_blue.png");
            pictureM.BackgroundImage = image;
        }

        private void pnlTop_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                moviendoFormulario = true;
                posicionActualPuntero = new Point(e.X, e.Y);
            }
            else
            {
                moviendoFormulario = false;
            }
        }

        private void pnlTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (moviendoFormulario)
            {
                Point nuevoPunto;

                nuevoPunto = PointToScreen(new Point(e.X, e.Y));
                nuevoPunto.Offset(-posicionActualPuntero.X, -posicionActualPuntero.Y);

                Location = nuevoPunto;
            }
        }

        private void pnlTop_MouseUp(object sender, MouseEventArgs e)
        {
            {
                moviendoFormulario = false;
            }
        }
    }
}
