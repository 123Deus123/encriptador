﻿namespace encriptar
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.btnEncriptar = new System.Windows.Forms.Button();
            this.btnDesencriptar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtDirectorio = new System.Windows.Forms.TextBox();
            this.lblDirectorio = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtGuardar = new System.Windows.Forms.TextBox();
            this.lblDestino = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureX = new System.Windows.Forms.PictureBox();
            this.pictureM = new System.Windows.Forms.PictureBox();
            this.lblEncriptador = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureM)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEncriptar
            // 
            this.btnEncriptar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEncriptar.Location = new System.Drawing.Point(141, 150);
            this.btnEncriptar.Name = "btnEncriptar";
            this.btnEncriptar.Size = new System.Drawing.Size(104, 36);
            this.btnEncriptar.TabIndex = 0;
            this.btnEncriptar.Text = "ENCRIPTAR";
            this.btnEncriptar.UseVisualStyleBackColor = true;
            this.btnEncriptar.Click += new System.EventHandler(this.btnEncriptar_Click);
            // 
            // btnDesencriptar
            // 
            this.btnDesencriptar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDesencriptar.Location = new System.Drawing.Point(292, 150);
            this.btnDesencriptar.Name = "btnDesencriptar";
            this.btnDesencriptar.Size = new System.Drawing.Size(104, 36);
            this.btnDesencriptar.TabIndex = 1;
            this.btnDesencriptar.Text = "DESENCRIPTAR";
            this.btnDesencriptar.UseVisualStyleBackColor = true;
            this.btnDesencriptar.Click += new System.EventHandler(this.btnDesencriptar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBuscar.Location = new System.Drawing.Point(423, 64);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 3;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtDirectorio
            // 
            this.txtDirectorio.Location = new System.Drawing.Point(100, 66);
            this.txtDirectorio.Name = "txtDirectorio";
            this.txtDirectorio.Size = new System.Drawing.Size(313, 20);
            this.txtDirectorio.TabIndex = 4;
            // 
            // lblDirectorio
            // 
            this.lblDirectorio.AutoSize = true;
            this.lblDirectorio.Location = new System.Drawing.Point(36, 69);
            this.lblDirectorio.Name = "lblDirectorio";
            this.lblDirectorio.Size = new System.Drawing.Size(52, 13);
            this.lblDirectorio.TabIndex = 5;
            this.lblDirectorio.Text = "ORIGEN:";
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardar.Location = new System.Drawing.Point(423, 104);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 6;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtGuardar
            // 
            this.txtGuardar.Location = new System.Drawing.Point(100, 106);
            this.txtGuardar.Name = "txtGuardar";
            this.txtGuardar.Size = new System.Drawing.Size(313, 20);
            this.txtGuardar.TabIndex = 7;
            // 
            // lblDestino
            // 
            this.lblDestino.AutoSize = true;
            this.lblDestino.Location = new System.Drawing.Point(36, 109);
            this.lblDestino.Name = "lblDestino";
            this.lblDestino.Size = new System.Drawing.Size(58, 13);
            this.lblDestino.TabIndex = 8;
            this.lblDestino.Text = "DESTINO:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(119)))), ((int)(((byte)(189)))));
            this.panel1.Controls.Add(this.lblEncriptador);
            this.panel1.Controls.Add(this.pictureM);
            this.panel1.Controls.Add(this.pictureX);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(526, 42);
            this.panel1.TabIndex = 9;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // pictureX
            // 
            this.pictureX.BackColor = System.Drawing.Color.Transparent;
            this.pictureX.BackgroundImage = global::encriptar.Properties.Resources.button_x_blue;
            this.pictureX.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureX.InitialImage")));
            this.pictureX.Location = new System.Drawing.Point(483, 3);
            this.pictureX.Name = "pictureX";
            this.pictureX.Size = new System.Drawing.Size(31, 31);
            this.pictureX.TabIndex = 1;
            this.pictureX.TabStop = false;
            this.pictureX.Click += new System.EventHandler(this.pictureX_Click);
            this.pictureX.MouseLeave += new System.EventHandler(this.pictureX_MouseLeave);
            this.pictureX.MouseHover += new System.EventHandler(this.pictureX_MouseHover);
            // 
            // pictureM
            // 
            this.pictureM.BackgroundImage = global::encriptar.Properties.Resources.button_min_blue;
            this.pictureM.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureM.InitialImage")));
            this.pictureM.Location = new System.Drawing.Point(442, 3);
            this.pictureM.Name = "pictureM";
            this.pictureM.Size = new System.Drawing.Size(31, 31);
            this.pictureM.TabIndex = 2;
            this.pictureM.TabStop = false;
            this.pictureM.MouseLeave += new System.EventHandler(this.pictureM_MouseLeave);
            this.pictureM.MouseHover += new System.EventHandler(this.pictureM_MouseHover);
            // 
            // lblEncriptador
            // 
            this.lblEncriptador.AutoSize = true;
            this.lblEncriptador.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEncriptador.ForeColor = System.Drawing.Color.White;
            this.lblEncriptador.Location = new System.Drawing.Point(183, 10);
            this.lblEncriptador.Name = "lblEncriptador";
            this.lblEncriptador.Size = new System.Drawing.Size(156, 24);
            this.lblEncriptador.TabIndex = 3;
            this.lblEncriptador.Text = "ENCRIPTADOR";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(526, 212);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblDestino);
            this.Controls.Add(this.txtGuardar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lblDirectorio);
            this.Controls.Add(this.txtDirectorio);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.btnDesencriptar);
            this.Controls.Add(this.btnEncriptar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Principal";
            this.Text = "Form2";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureM)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEncriptar;
        private System.Windows.Forms.Button btnDesencriptar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtDirectorio;
        private System.Windows.Forms.Label lblDirectorio;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtGuardar;
        private System.Windows.Forms.Label lblDestino;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureX;
        private System.Windows.Forms.PictureBox pictureM;
        private System.Windows.Forms.Label lblEncriptador;
    }
}