﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Modelos
{
    public class Documentos
    {
        [Key]
        public int Id { get; set; }
        public string cedula { get; set; }
        public string ciudad_nacimiento { get; set; }
        public string fecha_expedicion { get; set; }
    }
}
