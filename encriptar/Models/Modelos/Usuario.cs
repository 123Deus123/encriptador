﻿using Models.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Models.Models
{
    public class Usuario 
    {
        [Key]
        public int Id { get; set; }
        public string Users { get; set; }
        public string Password { get; set; }
        public int idDocumento { get; set; }
        [ForeignKey("idDocumento")]
        public virtual Documentos Documentos { get; set; }
    }
}
