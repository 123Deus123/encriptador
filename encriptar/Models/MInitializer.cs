﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class MInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<MContext>
    {
        public void Ini(MContext context)
        {
            Seed(context);
        }
        protected override void Seed(MContext context)
        {
            context.SaveChanges();
        }
    }
}
