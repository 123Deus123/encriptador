﻿using Models.Modelos;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class MContext: DbContext
    {
        public MContext() : base("MContext") { }

        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Documentos> Documentos { get; set; }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                //var newException = new FormattedDbEntityValidationException(e);
                //throw newException;
                return 0;
            }
        }
    }
}
